For two given days, an optimization problem is reported on this precept. The first is a typical winter day, characterized by a large thermal power use, and the second is a typical midseason day, where the heat demand is not so large.
Considering the given electric and thermal power demands for these two given days, a Mixed-Integer Linearization Program (MILP) was written using Pyomo (a python library for optimization problems) to determine the optimal operating strategy of the Multi-Energy System (MES) that minimizes the total operating costs and provide the following plots:

 Overall electricity balance for each time step of the day (electric demand, electricity produced/consumed by each unit, purchased/sold electricity, electricity charged/discharged from the battery, battery state of charge);

 Overall thermal energy balance for each time step of the day (heat demand, heat produced by each unit, storage charge/discharge, storage level of charge).

For that, the MES optimization MILP model was based on the following steps:
1) Create an instance of a model using Pyomo modeling components;
2) Pass this instance to a solver to find a solution;
3) Report and Analyze results from the solver.
For each day, the optimization problem was solved for the following cases and the main effects in terms of objective function and operating strategy of the units were described.
a) No electric and thermal storages available
b) Thermal storage with capacity equal to 2 MWh, no battery
c) Thermal storage with capacity equal to 2 MWh, and battery with capacity equal to 1 MWh
d) Thermal storage with capacity equal to 4 MWh, and battery with capacity equal to 1 MWh