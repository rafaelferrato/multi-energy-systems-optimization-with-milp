"""
PRECEPT 2 - MULTI-ENERGY SYSTEMS (MES)

"""
#first we need to import the Pyomo library
import pyomo.environ as pyo #each function in this environment is reported with the name pyo

#import pandas for the excel files you're working with
import pandas as pd

#import numpy for arrays 
import numpy as np

#import package to draw plots
import matplotlib.pyplot as plt

#declare the models used in the exercise
model = pyo.ConcreteModel() 

#Define the Sets:
# Units = ["ICE", "Boiler1", "Boiler2", "HeatPump"]
# Units_el = ["ICE"]
# Units_fuel = ["ICE", "Boiler1", "Boiler2"]
# Units_thermal = Units
# Units_cons_el = ["HeatPump"]
# print(Units)
# print (Units_fuel)
# print (Units_el)

#SET
model.I = pyo.Set(initialize = ["ICE","HeatPump", "Boiler1", "Boiler2"])
model.I_f = pyo.Set(within = model.I, initialize = ["ICE", "Boiler1", "Boiler2"]) #subset of I for machines that use FUEL
model.I_el = pyo.Set(within = model.I, initialize = ["ICE"]) #subset of I for machines that produce Elec
model.I_th = pyo.Set(within = model.I, initialize = ["ICE","HeatPump", "Boiler1", "Boiler2"])    #Units that produce heat
model.I_cons_el = pyo.Set(within = model.I, initialize = ["HeatPump"]) #consume electricity

#Time instants Set
T = 24
model.T = pyo.RangeSet(0,T-1) #RangeSet is equal to a list with range from 0 to T-1 = 0 to 23 hours

#Storage Systems
model.Storage = pyo.Set(initialize = ['TES','BAT'])
model.Storage_el = pyo.Set(within = model.Storage, initialize = ['BAT'])
model.Storage_th = pyo.Set(within = model.Storage, initialize = ['TES'])

#VARIABLES
model.Fuel = pyo.Var(model.I_f, model.T, domain = pyo.NonNegativeReals) #positive and non negative
model.Q = pyo.Var(model.I_th, model.T, domain = pyo.NonNegativeReals) #positive and non negative
model.P = pyo.Var(model.I_el, model.T, domain = pyo.NonNegativeReals) #positive and non negative
model.el_in = pyo.Var(model.I_cons_el, model.T, domain = pyo.NonNegativeReals) #positive and non negative

#BINARY VARIABLES
model.z = pyo.Var(model.I, model.T, domain = pyo.Binary) #on and off variables
model.d_SU = pyo.Var(model.I, model.T, domain = pyo.Binary) #Start-up variables

#Storage VARIABLES
model.U = pyo.Var(model.Storage, model.T, domain = pyo.NonNegativeReals)
model.Charge = pyo.Var(model.Storage, model.T, domain = pyo.NonNegativeReals)
model.Discharge = pyo.Var(model.Storage, model.T, domain = pyo.NonNegativeReals)

#Market VARIABLES
model.el_bought = pyo.Var(model.T, domain = pyo.NonNegativeReals)
model.el_sold = pyo.Var(model.T, domain = pyo.NonNegativeReals)

#DATA
#Demand of electricity and heat
day_2_df = pd.read_csv("day_2.csv", sep=",", index_col = 0, header = 0)
#print (day_2_df)
d_el = list(day_2_df["d_el"])    #data from excel file
#print (d_el)
d_th = list(day_2_df["d_th"])
#print (d_th)
#Electricity buying and selling prices
el_buy = list(day_2_df["El_price_buy"])
#print (el_buy)
el_sell = list(day_2_df["El_price_sell"])
#print (el_sell)
#Natural Gas Price
NG_price = 0.03 #constant

#PARAMETERS FOR THE UNITS
Fuel_cost = {"ICE":NG_price,"Boiler1":NG_price,"Boiler2":NG_price}
a_el = {"ICE":0.49}
b_el = {"ICE":-222.122682926829}
a_th = {"ICE":0.439,"HeatPump":3.59,"Boiler1":0.976,"Boiler2":0.945}
b_th= {"ICE":-166.210243902439,"HeatPump":-68.3760683760684,"Boiler1":-50.8474576271186,"Boiler2":-36.8017524644031}
OM_cost = {"ICE": 14.5648928402855,"HeatPump":17.7123711055263,"Boiler1":0,"Boiler2":0}
SU_cost = {"ICE":12.6700718966145,"HeatPump": 12.1686223381139, "Boiler1":2.40041870832999,"Boiler2":1.73734576392602}
MinIn = {"ICE":2743.90243902439, "HeatPump":111.111111111111,"Boiler1":397.245762711864,"Boiler2":287.513691128149}
MaxIn = {"ICE":5487.80487804878,"HeatPump":854.700854700855,"Boiler1":1588.98305084746,"Boiler2":1150.0547645126}

#STORAGE PARAMETERS
MaxC = {"TES":0,"BAT":0}    #MaxC = 0 means NO STORAGE (letter a) in kWh
eta_charge = {"BAT":0.97}
eta_disch = {"BAT":0.97}
eta_diss = {"TES":0.995}

#OBJECTIVE FUNCTION
def obj_function(model):
    machines_fuel_cost = sum(Fuel_cost[i]*model.Fuel[i,t] for i in model.I_f for t in model.T)
    machines_OM_cost = sum(OM_cost[i]*model.z[i,t] for i in model.I for t in model.T)
    machines_SU_cost = sum(SU_cost[i]*model.d_SU[i,t] for i in model.I for t in model.T)
    grid_Sell_Buy = sum(el_buy[t]*model.el_bought[t] - el_sell[t]*model.el_sold[t] for t in model.T)
    return machines_fuel_cost + machines_OM_cost + machines_SU_cost + grid_Sell_Buy
model.obj = pyo.Objective(rule = obj_function, sense = pyo.minimize)

##CONTRAINTS
#Electricity Balance
def el_balance_rule(model,t):
    return sum(model.P[i,t] for i in model.I_el) - sum(model.el_in[i,t] for i in model.I_cons_el) + model.el_bought[t] - model.el_sold[t] + sum(model.Discharge[s,t] for s in model.Storage_el) - sum(model.Charge[s,t] for s in model.Storage_el) == d_el[t]
model.el_balance_constraint = pyo.Constraint(model.T, rule = el_balance_rule)

#Thermal Balance
def th_balance_rule(model,t):
    return sum(model.Q[i,t] for i in model.I_th) + sum(model.Discharge[s,t] for s in model.Storage_th) - sum(model.Charge[s,t] for s in model.Storage_th) == d_th[t]
model.th_balance_constraint = pyo.Constraint(model.T, rule = th_balance_rule)

#Performance Maps Constraints:
def power_prod_rule(model,i,t):
    return model.P[i,t] == a_el[i]*model.Fuel[i,t] + b_el[i]*model.z[i,t]
model.power_prod_contraint = pyo.Constraint(model.I_el, model.T, rule = power_prod_rule)

def th_prod_rule(model,i,t):
    if i in model.I_f:
        return model.Q[i,t] == a_th[i]*model.Fuel[i,t] + b_th[i]*model.z[i,t]
    elif i in model.I_cons_el:
        return model.Q[i,t] == a_th[i]*model.el_in[i,t] + b_th[i]*model.z[i,t]
model.th_prod_constraint = pyo.Constraint(model.I_th, model.T, rule = th_prod_rule)

##OPERATING RANGE
#minimum input
def min_input_rule(model,i,t):
    if i in model.I_f:
        return model.Fuel[i,t] >= MinIn[i]*model.z[i,t]
    elif i in model.I_cons_el:
        return model.el_in[i,t] >= MinIn[i]*model.z[i,t]
model.min_input_constraint = pyo.Constraint(model.I, model.T, rule = min_input_rule)

#max input 
def max_input_rule(model,i,t):
    if i in model.I_f:
        return model.Fuel[i,t] <= MaxIn[i]*model.z[i,t]
    elif i in model.I_cons_el:
        return model.el_in[i,t] <= MaxIn[i]*model.z[i,t]
model.max_input_constraint = pyo.Constraint(model.I, model.T, rule = max_input_rule)

##LOGICAL CONSTRAINTS
#These constraints are needed to correctly calculate the values
def logical_SU_rule(model,i,t):
    if t > 0:
        return model.z[i,t] - model.z[i,t-1] <= model.d_SU[i,t]
    elif t == 0:
        return model.z[i,0] - model.z[i,T-1] <= model.d_SU[i,0] #we explicit say it is T = circularity: the beginning of the day = end of last day
model.logical_SU_constraint = pyo.Constraint(model.I, model.T, rule = logical_SU_rule)

##STORAGE CONSTRAINTS
#Thermal Storage En. Balance
def th_storage_rule(model,s,t):
    if t > 0:
        return model.U[s,t] - model.U[s, t-1]*eta_diss[s] == model.Charge[s,t] - model.Discharge[s,t]
    elif t == 0:
        return model.U[s,0] - model.U[s, T-1]*eta_diss[s] == model.Charge[s,0] - model.Discharge[s,0]
model.th_storage_constraint = pyo.Constraint(model.Storage_th, model.T, rule = th_storage_rule)

#Electrical Storage En. Balance
def el_storage_rule(model,s,t):
    if t > 0:
        return model.U[s,t] - model.U[s, t-1] == model.Charge[s,t]*eta_charge[s] - model.Discharge[s,t]/eta_disch[s]
    elif t == 0:
        return model.U[s,0] - model.U[s, T-1] == model.Charge[s,0]*eta_charge[s] - model.Discharge[s,0]/eta_disch[s]
model.el_storage_constraint = pyo.Constraint(model.Storage_el, model.T, rule = el_storage_rule)

def capacity_rule(model,s,t):
    return model.U[s,t] <= MaxC[s]
model.capacity_constraint = pyo.Constraint(model.Storage, model.T, rule = capacity_rule)

#SOLVER
pyo.SolverFactory('CBC',mipgap = 0.005).solve(model).write()
#we call a solve = CBC
#we assign a gap for convergence = 0.5%
#solve(model).write() = solve model and save

##PRINTING GRAPHS AND RESULTS
#Save the Results in empty dictionaries
electricity = {}
electricity_cons = {}
electricity_ch = {}
electricity_dis = {}
heat = {}
heat_ch = {}
heat_dis = {}
level_th = {}
level_el = {}

for i in model.I.value:
    if i in model.I_el:
        electricity[i] = np.array(list(model.P[i,:].value))   #store the value from model P in the dictionary for each unit
    if i in model.I_th:
        heat[i] = np.array(list(model.Q[i,:].value))
    if i in model.I_cons_el:
        electricity_cons[i] = np.array(list(model.el_in[i,:].value))
for s in model.Storage.value:
    if s in model.Storage_el:
        electricity_ch[s] = np.array(list(model.Charge[s,:].value))
        electricity_dis[s] = np.array(list(model.Discharge[s,:].value))
        level_el[s] = np.array(list(model.U[s,:].value))
    if s in model.Storage_th:
        heat_ch[s] = np.array(list(model.Charge[s,:].value))
        heat_dis[s] = np.array(list(model.Discharge[s,:].value))
        level_th[s] = np.array(list(model.U[s,:].value))
electricity_buy = np.array(list(model.el_bought[:].value))
electricity_sell = np.array(list(model.el_sold[:].value))

##PLOTS
times = range(T)

#Electricity plot:
barplot = plt.figure() #bar plot
#adding electricity produced by machines
cm_el = 0
for i in model.I_el:
    plt.bar(times, height = electricity[i], bottom = cm_el, label = i)
    cm_el += np.array(electricity[i]) #add to previous value the current value of elec.
#subtract electricity consumed by machines
cm_el_neg = 0
for i in model.I_cons_el:
    plt.bar(times, height = -electricity_cons[i], bottom = -cm_el_neg, label = i)
    cm_el_neg += np.array(electricity_cons[i]) #this will add negative bars and will be summed negatively
#grid
plt.bar(times, height = electricity_buy, bottom = cm_el, label = 'Grid Buy')
cm_el += electricity_buy
plt.bar(times, height = -electricity_sell, bottom = -cm_el_neg, label = 'Grid Sell')
cm_el_neg += electricity_sell
#Adding Battery disch/charge
for s in model.Storage_el:
    plt.bar(times, height = electricity_dis[s], bottom = cm_el, label = s+'discharge')
    cm_el += np.array(electricity_dis[s])
    plt.bar(times, height = -electricity_ch[s], bottom = -cm_el_neg, label = s+'charge')
    cm_el_neg += np.array(electricity_ch[s])

#Electricity Demand
plt.plot(times, d_el, '--k', label = 'El Demand')
#Battery Level
for s in model.Storage_el:
    plt.plot(times, level_el[s], '--r', label = 'level'+s)
plt.legend(loc = 'upper center', bbox_to_anchor = (0.5,-.08))
plt.title("Electricity Profiles")
plt.ylabel("kWh")

#HEAT
barplot = plt.figure()
#Adding HEAT PROD by machines
cm_th = 0
for i in model.I_th:
    plt.bar(times, height = heat[i], bottom = cm_th, label = i)
    cm_th += np.array(heat[i])
#Adding Storage disch/charge
cm_th_neg = 0
for s in model.Storage_th:
    plt.bar(times, height = heat_dis[s], bottom = cm_th, label = s+ 'discharge')
    cm_th += np.array(heat_dis[s])
    plt.bar(times, height = -heat_ch[s], bottom = -cm_th_neg, label = s+ 'charge')
    cm_th_neg += np.array(heat_ch[s])

#Heat Demand
plt.plot(times, d_th, '--k', label = "Heat Demand")
#TES level
for s in model.Storage_th:
    plt.plot(times, level_th[s], '--r', label = "level"+s)
plt.legend(loc = 'upper center', bbox_to_anchor = (0.5, -0.08))
plt.title("Heat Profiles")
plt.ylabel("kWh")

#Gettin the Results in Excel Tables
Table_el = {"Hour":times, "Power_prod":electricity["ICE"], "Power_cons":electricity_cons["HeatPump"], "BAT_ch":electricity_ch["BAT"], "BAT_dis":electricity_dis["BAT"],"Level_BAT":level_el["BAT"]}
El_df = pd.DataFrame(Table_el, columns=Table_el.keys())

i = heat.keys()
#print (i)
Table_th = {"Hour":times}
for i in heat:
    Table_th[i] = heat[i]
Table_th["TES_ch"] = heat_ch["TES"]
Table_th["TES_dis"] = heat_dis["TES"]
Table_th["Level_TES"] = level_th["TES"]
Th_df = pd.DataFrame(Table_th, columns=Table_th.keys())

El_df.to_csv(r'C:\Users\Rafael Ferrato\Documents\CLASSES_Politecnico_Milano\2nd_Semester\Low_Carbon_Technologies\Exercices\Precept_2-MES\Day_2_El_No_Storage.csv', header = True, sep =",")
Th_df.to_csv(r'C:\Users\Rafael Ferrato\Documents\CLASSES_Politecnico_Milano\2nd_Semester\Low_Carbon_Technologies\Exercices\Precept_2-MES\Day_2_Th_No_Storage.csv', header = True, sep =",")

Bought_el = 0
for t in model.T:
    Bought_el_hour = electricity_buy[t]*el_buy[t]
    Bought_el += Bought_el_hour
print ("MES spent ", Bought_el,"euros in energy")

Sold_el = 0
for t in model.T:
    Sold_el_hour = electricity_sell[t]*el_sell[t]
    Sold_el += Sold_el_hour
print("MES Sold ", Sold_el, "euros of energy")

Revenue = Sold_el - Bought_el
print ("MES Revenue is of ", Revenue, "euros")

